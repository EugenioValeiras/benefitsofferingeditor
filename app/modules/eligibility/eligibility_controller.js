EligibilityApp.module("Eligibility", function(Eligibility, App, Backbone, Marionette, $, _) {
    var EligibilityController = Marionette.Object.extend({
        show: function() {
           var self = this;
           this.loadData().done(function() {
               self.initPage();
           });
        },
        loadData: function() {
            this.collection = new App.Entities.EligibilityCollection();
            this.collection.fetch();

            this.plans = new App.Entities.PlanCollection();

            return this.plans.fetch();
        },
        initPage: function() {
            this.view = new Eligibility.MainView({collection: this.collection});
            App.mainContent.show(this.view);
        },
        remove: function(model) {
            this.collection.deleteItem(model);
        },
        getPlans: function() {
            return this.plans;
        },
        addNew: function() {
            var self = this;
            var newModel = new App.Entities.Eligibility();
            newModel.save(null, {success: function() {
                self.collection.addNew(newModel);
            }});
        },
        synCollection: function(model, newOrder) {
            this.collection.syncOrder(model, newOrder);
        }
    });

    this.addInitializer(function() {
        this.Controller = new EligibilityController();
    });

});